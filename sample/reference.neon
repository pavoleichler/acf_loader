fields:
    # FIELD TYPES

    # Shortcuts notations
    # text fields, add an asterisk to mark as required
    shortcut_text_optional: Text field
    shortcut_text_required: Text field *

    # add <ta> to create a text area, add an asterisk to mark as required
    shortcut_textarea_optional: Text area <ta>
    shortcut_textarea_required: Text area <ta>*

    # use more shortcuts with a <t> syntax, add an astrerisk to mark as required
    shortcut_wysiwyg_optional: WYSIWYG <w>
    shortcut_image_optional: Image <i>

    # use any other type providing its full type name with a <type> syntax, e.g.:
    shortcut_image_optional_2: Image <image>
    shortcut_tab: Tab <tab>
    shortcut_wysiwyg_optional_2: Link <wysiwyg>
    shortcut_link_optional: Link <link>
    shortcut_link_required: Link <link>*

    # Full object notations with all available settings
    # Basic
    text:
        type: text
        label: Text field
        maxlength: 100
        required: false
        instructions: This is a short hint.
        default_value: Default value for new posts.
        placeholder: Placeholder
        prepend: Content before the input
        append: Content after the input
    textarea:
        type: textarea
        label: Text area
        maxlength: 160
        rows: 5
        new_lines: wpautop # or 'br' or null to keep \n with no change
        required: false
        instructions: This is a short hint.
        default_value: Default value for new posts.
        placeholder: Placeholder
    number:
        type: number
        label: Number field
        min: 10
        max: 100
        step: 5
        required: false
        instructions: This is a short hint.
        default_value: Default value for new posts.
        placeholder: Placeholder
        prepend: Content before the input
        append: Content after the input
    range:
        type: range
        label: Range field
        min: 10
        max: 100
        step: 5
        required: false
        instructions: This is a short hint.
        default_value: Default value for new posts.
        prepend: Content before the input
        append: Content after the input
    email:
        type: email
        label: Email field
        required: false
        instructions: This is a short hint.
        default_value: Default value for new posts.
        placeholder: Placeholder
        prepend: Content before the input
        append: Content after the input
    url:
        type: url
        label: URL field
        required: false
        instructions: This is a short hint.
        default_value: Default value for new posts.
        placeholder: Placeholder
    password:
        type: password
        label: Password field
        required: false
        instructions: This is a short hint.
        placeholder: Placeholder
        prepend: Content before the input
        append: Content after the input

    # Content
    image:
        type: image
        label: Image upload
        return_format: id # or 'url' / 'array'
        library: all # or 'uploadedTo'
        preview_size: thumbnail
        min_height: 1000
        min_size: 1000 # (in MB)
        max_width: 1000
        max_height: 1000
        max_size: 1000 # (in MB)
        mime_types: 'jpg,png,svg' # or empty for all types
        required: false
        instructions: This is a short hint.
    file:
        type: file
        label: File upload
        return_format: id # or 'url' / 'array'
        library: all # or 'uploadedTo'
        min_size: 1000 # (in MB)
        max_size: 1000 # (in MB)
        mime_types: mp4 # or empty for all types
        required: false
        instructions: This is a short hint.
    wysiwyg:
        label: WYSIWYG editor
        type: wysiwyg
        toolbar: full # or 'basic' / 'simple' / 'very_simple' or see theme.neon for more options
        tabs: all # or 'visual' or 'text'
        media_upload: false
        delay: false
        required: false
        instructions: This is a short hint.
        default_value: Default value for new posts.
    oembed:
        label: oEmbed
        type: oembed
        width: 640
        height: 480
        required: false
        instructions: This is a short hint.
    gallery:
        label: Gallery
        type: gallery
        return_format: id # or 'url' / 'array'
        preview_size: thumbnail
        insert: append # or 'prepend'
        library: all # or 'uploadedTo'
        min: 3
        max: 12
        min_height: 1000
        min_size: 1000 # (in MB)
        max_width: 1000
        max_height: 1000
        max_size: 1000 # (in MB)
        mime_types: mp4 # or empty for all types
        required: false
        instructions: This is a short hint.

    # Choice
    select:
        type: select
        label: Select
        choices:
            value_1: Label 1
            value_2: Label 2
            value_3: Label 3
        allow_null: false
        multiple: false
        ui: true # stylised UI
        return_format: value # or 'label' / 'array'
        required: false
        instructions: This is a short hint.
        default_value: value_2
    checkbox:
        type: checkbox
        label: Checkbox list
        choices:
            value_1: Label 1
            value_2: Label 2
            value_3: Label 3
        allow_custom: false
        layout: vertical # or 'horizontal'
        toggle: false
        return_format: value # or 'label' / 'array'
        required: false
        instructions: This is a short hint.
        default_value: value_2
    radio:
        type: radio
        label: Radio button list
        choices:
            value_1: Label 1
            value_2: Label 2
            value_3: Label 3
        allow_null: false
        other: false
        layout: vertical # or 'horizontal'
        return_format: value # or 'label' / 'array'
        required: false
        instructions: This is a short hint.
        default_value: value_2
    button_group:
        type: button_group
        label: Button group
        choices:
            value_1: Label 1
            value_2: Label 2
            value_3: Label 3
        layout: vertical # or 'horizontal'
        return_format: value # or 'label' / 'array'
        required: false
        instructions: This is a short hint.
        default_value: value_2
    true_false:
        type: true_false
        label: True or false
        message: This is a short message
        ui: true
        required: false
        instructions: This is a short hint.
        default_value: true

    # Relational
    link:
        type: link
        label: Link
        return_format: array # or 'url'
        required: false
        instructions: This is a short hint.
    post_object:
        type: post_object
        label: Post Object
        post_type: post # or any other existing post type
        taxonomy: 1 # taxonomy ID
        allow_null: false
        multiple: false
        return_format: object # or 'id'
        required: false
        instructions: This is a short hint.
    page_link:
        type: page_link
        label: Page Link
        post_type: post # or any other existing post type
        taxonomy: 1 # taxonomy ID
        allow_null: false
        allow_archives: true
        multiple: false
        required: false
        instructions: This is a short hint.
    relationship:
        type: relationship
        label: Relationship
        post_type: post # or any other existing post type
        taxonomy: 1 # taxonomy ID
        filters: search # or 'post_type' / 'taxonomy'
        elements: featured_image # or empty
        min: 3
        max: 6
        return_format: object # or 'id'
        required: false
        instructions: This is a short hint.
    taxonomy:
        type: taxonomy
        label: Taxonomy
        taxonomy: category # or any other existing taxonomy
        field_type: checkbox # or 'multi_select' / 'radio' / 'select'
        add_term: true
        save_terms: false
        load_terms: false
        return_format: object # or 'id'
        required: false
        instructions: This is a short hint.
    user:
        type: user
        label: User
        role: editor # or any other existing role
        allow_null: false
        multiple: false
        return_format: object # or 'id' / 'array'
        required: false
        instructions: This is a short hint.

    # jQuery
    google_map:
        type: google_map
        label: Google map
        # for more properties, see Custom Fields settings in WP admin
    date_picker:
        type: date_picker
        label: Date picker
        # for more properties, see Custom Fields settings in WP admin
    date_time_picker:
        type: date_time_picker
        label: Datetime picker
        # for more properties, see Custom Fields settings in WP admin
    time_picker:
        type: time_picker
        label: Time picker
        # for more properties, see Custom Fields settings in WP admin
    color_picker:
        type: color_picker
        label: Color picker
        # for more properties, see Custom Fields settings in WP admin

    # Layout
    message:
        type: message
        label: Message
        message: This is a short read-only message.
        new_lines: wpautop # or 'br' or null to keep \n with no change
        esc_html: false
    accordion:
        type: accordion
        label: Accordion
        open: false # display this accordion as open on page load
        multi_expand: false # allow this accordion to open without closing others
        endpoint: false # define an endpoint for the previous accordion to stop, this accordion will not be visible
        instructions: This is a short hint.
    tab:
        type: tab
        label: Tab
        placement: top # or 'left'
        endpoint: false # define an endpoint for the previous tabs to stop, this will start a new group of tabs
    group:
        type: group
        label: Group
        layout: block # or 'table' / 'row'
        sub_fields:
            # list of child fields for this group
            foo_1: Foo 1
            foo_2:
                label: Foo 2
                type: textarea
        required: false
        instructions: This is a short hint.
    repeater:
        type: repeater
        label: Repeater
        layout: block # or 'table' / 'row'
        min: 3
        max: 6
        sub_fields:
            # list of child fields for this group
            foo_1: Foo 1
            foo_2:
                label: Foo 2
                type: textarea
        collapsed: foo_1 # select a sub field to show when row is collapsed
        button_label: Add Row
        required: false
        instructions: This is a short hint.
    flexible_content:
        type: flexible_content
        label: Flexible content
        min: 3
        max: 6
        layouts:
            # list of available layouts
            foo_layout_1:
                label: Text
                layout: block # or 'table' / 'row'
                min: 3
                max: 6
                sub_fields:
                    # list of child fields for this layout
                    foo_1: Foo 1
                    foo_2:
                        label: Foo 2
                        type: textarea
        button_label: Add Row
        required: false
        instructions: This is a short hint.

    # ADVANCED
    # Conditional logic available for all fields
    conditional_logic_checkbox:
        type: true_false
        label: Show one more field?
        required: false
        ui: true
    conditional_logic_input:
        type: text
        label: One more field
        show_if:
            field: conditional_logic_checkbox
            operator: '=='
            value: 1