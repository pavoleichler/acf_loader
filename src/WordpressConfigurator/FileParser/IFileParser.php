<?php

namespace WordpressConfigurator\FileParser;

interface IFileParser
{

    public function parse($file);

}