<?php

namespace WordpressConfigurator\FileParser;

class Neon
{

    public function parse($file)
    {

        return \Nette\Neon\Neon::decode(file_get_contents($file));

    }

}