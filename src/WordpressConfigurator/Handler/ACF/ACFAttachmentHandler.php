<?php

namespace WordpressConfigurator\Handlers\ACF;

use \WordpressConfigurator\Handlers\ACF\Field\FieldFactory;
use \WordpressConfigurator\Handlers\ACF\Group\Group;

class ACFAttachmentHandler extends ACFHandler
{

    public function run($config, $context)
    {

        // extract page from the file name
        $slug = basename($context->file, '.neon');

        // add a page template location
        $config['location'][] = [
            [
                'param' => 'attachment',
                'operator' => '==',
                'value' => $slug,
            ]
        ];

        $group = new Group($this->createGroupId($context, 'attachment-' . $slug), $config, new FieldFactory($this->defaults));
        $group->setup();

    }

}