<?php

namespace WordpressConfigurator\Handlers\ACF;

use \WordpressConfigurator\Handlers\ACF\Field\FieldFactory;
use \WordpressConfigurator\Handlers\ACF\Group\Group;

class ACFBlockTypeHandler extends ACFHandler
{

    /**
     * @var callable
     */
    protected $render;
    protected $blocks = [
        'categories' => [],
        'blocks' => [],
    ];

    public function __construct(callable $render, $blockCategories = [], $defaults = [])
    {
        parent::__construct($defaults);

        // save settings
        $this->render = $render;
        $this->blocks['categories'] = $blockCategories;

        // bind block registration
        add_action('acf/init', [$this, 'registerBlocks']);

    }

    public function run($config, $context)
    {

        // extract name from the file name
        $slug = basename($context->file, '.neon');

        // block settings
        $renderCallback = $this->render;
        $this->blocks['blocks'][$slug] = [
            'name'              => $slug,
            'title'             => __($config['title']),
            'description'       => !empty($config['description']) ? __($config['description']) : null,
            'render_callback'   => function($block) use ($renderCallback) { $renderCallback($block); },
            'category'          => $config['category'],
            'icon'              => !empty($config['icon']) ? $config['icon'] : null,
            'supports'          => !empty($config['supports']) ? $config['supports'] : null,
            'align_content'     => !empty($config['align_content']) ? $config['align_content'] : null,
            'align_text'        => !empty($config['align_text']) ? $config['align_text'] : null,
            'align'             => !empty($config['align']) ? $config['align'] : null,
            'mode'              => !empty($config['mode']) ? $config['mode'] : null,
            'post_types'        => !empty($config['post_types']) ? $config['post_types'] : null,
            'keywords'          => !empty($config['keywords']) ? $config['keywords'] : null,
            'example'           => !empty($config['example']) ? $config['example'] : null,
        ];

        // add a page template location
        $config['location'][] = [
            [
                'param' => 'block',
                'operator' => '==',
                'value' => 'acf/' . $slug,
            ]
        ];

        // create fields
        $group = new Group($this->createGroupId($context, 'block-' . $slug), array_diff_key($config, $this->blocks['blocks'][$slug]), new FieldFactory($this->defaults));
        $group->setup();

    }

    public function registerBlocks()
    {

        // block categories
        add_filter('block_categories', function( $categories, $post ){
            $categories = array_merge($categories, $this->blocks['categories']);
            usort($categories, function($a, $b){
                $aOrder = $a['order'] ?? 0;
                $bOrder = $b['order'] ?? 0;
                return $aOrder - $bOrder;
            });
            return $categories;
        }, 10, 2 );

        // block types
        if (function_exists('acf_register_block_type')){
            foreach($this->blocks['blocks'] as $config){
                acf_register_block_type($config);
            }
        }

    }

}