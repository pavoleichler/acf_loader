<?php

namespace WordpressConfigurator\Handlers\ACF;

use WordpressConfigurator\Handlers\ACF\Field\FieldFactory;
use WordpressConfigurator\Handlers\ACF\Group\Group;
use WordpressConfigurator\Handlers\IHandler;

class ACFHandler implements IHandler
{

    protected $defaults;

    public function __construct($defaults = [])
    {

        $this->defaults = $defaults;

    }

    public function run($config, $context)
    {

        $group = new Group(basename($context['file'], '.neon'), $config, new FieldFactory($this->defaults));
        $group->setup();

    }

    protected function createGroupId($context, $slug)
    {

        return $context->theme . '-' . $slug;

    }

}