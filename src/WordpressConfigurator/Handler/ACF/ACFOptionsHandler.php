<?php

namespace WordpressConfigurator\Handlers\ACF;

use ACFLoader\Configurator\Options\Page;
use WordpressConfigurator\Handlers\ACF\Field\FieldFactory;
use WordpressConfigurator\Handlers\ACF\Group\Group;

class ACFOptionsHandler extends ACFHandler
{

    public function run($config, $context)
    {

        // extract page from the file name
        $slug = basename($context->file, '.neon');

        // create a settings page
        $options = new Page($slug, $config['post_id'], $config['title'], $config['icon']);
        $options->setup();

        // add settings location
        $config['location'][] = [[
            "param" => "options_page",
            "operator" => '==',
            "value" => $slug
        ]];

        // create group
        $group = new Group($this->createGroupId($context, 'options-' . $slug), $config, new FieldFactory($this->defaults));
        $group->setup();

    }

}