<?php

namespace WordpressConfigurator\Handlers\ACF;

use WordpressConfigurator\Handlers\ACF\Field\FieldFactory;
use WordpressConfigurator\Handlers\ACF\Group\Group;
use WordpressConfigurator\Handlers\IHandler;

class ACFPageHandler extends ACFHandler
{

    protected $blocks = [];

    public function __construct($defaults = [])
    {
        parent::__construct($defaults);

        // bind block settings
        add_filter( 'gutenberg_can_edit_post_type', [$this, 'useBlockEditor'], 10, 2 );
        add_filter( 'use_block_editor_for_post_type', [$this, 'useBlockEditor'], 10, 2 );
        add_action('allowed_block_types', [$this, 'allowedBlocks'], 10, 2);

    }

    public function run($config, $context)
    {

        // extract page from the file name
        $page = basename($context->file, '.neon');

        // add a page template location
        $config['location'][] = [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'page',
            ],
            [
                'param' => 'post_template',
                'operator' => '==',
                'value' => 'page-' . $page . '.php',
            ]
        ];

        // save block settings
        $this->blocks['page-' . $page . '.php'] = [
            'block_editor' => !empty($config['block_editor']) ? $config['block_editor'] : true,
            'allowed_block_types' => !empty($config['allowed_block_types']) ? $config['allowed_block_types'] : [],
        ];

        $group = new Group($this->createGroupId($context, 'page-' . $page), $config, new FieldFactory($this->defaults));
        $group->setup();

    }

    public function useBlockEditor($canEdit, $postType)
    {
        global $post;
        if ($post and !empty($this->blocks[get_page_template_slug($post)]) and $this->blocks[get_page_template_slug($post)]['block_editor'] === false){
            return false;
        }
        return $canEdit;
    }

    public function allowedBlocks($allowedBlocks, $post)
    {

        if (!empty($this->blocks[get_page_template_slug($post)])){
            $allowedBlocks = is_array($allowedBlocks) ? $allowedBlocks : [];
            return array_merge($allowedBlocks, $this->blocks[get_page_template_slug($post)]['allowed_block_types']);
        }

        return $allowedBlocks;

    }

}