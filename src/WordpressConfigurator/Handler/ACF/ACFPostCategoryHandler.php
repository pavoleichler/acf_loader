<?php

namespace WordpressConfigurator\Handlers\ACF;

use \WordpressConfigurator\Handlers\ACF\Field\FieldFactory;
use \WordpressConfigurator\Handlers\ACF\Group\Group;

class ACFPostCategoryHandler extends ACFHandler
{

    protected $category;

    public function run($config, $context)
    {

        // extract page from the file name
        $category = basename($context->file, '.neon');

        // add a page template location
        $config['location'][] = [
            [
                'param' => 'post_category',
                'operator' => '==',
                'value' => 'category:' . $category,
            ]
        ];

        $group = new Group($this->createGroupId($context, 'post-category-' . $category), $config, new FieldFactory($this->defaults));
        $group->setup();

    }

}