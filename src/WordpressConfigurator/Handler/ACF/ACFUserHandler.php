<?php

namespace WordpressConfigurator\Handlers\ACF;

use \WordpressConfigurator\Handlers\ACF\Field\FieldFactory;
use \WordpressConfigurator\Handlers\ACF\Group\Group;

class ACFUserHandler extends ACFHandler
{

    protected $formId;

    public function __construct($formId, $defaults)
    {
        parent::__construct($defaults);

        $this->formId = $formId;
    }

    public function run($config, $context)
    {

        // extract page from the file name
        $slug = basename($context->file, '.neon');

        // add a page template location
        $config['location'][] = [
            [
                'param' => 'user_form',
                'operator' => '==',
                'value' => $this->formId,
            ]
        ];

        $group = new Group($this->createGroupId($context, 'user-' . $slug), $config, new FieldFactory($this->defaults));
        $group->setup();

    }

}