<?php

namespace WordpressConfigurator\Handlers\ACF;

use \WordpressConfigurator\Handlers\ACF\Field\FieldFactory;
use \WordpressConfigurator\Handlers\ACF\Group\Group;

class ACFUserRoleHandler extends ACFHandler
{

    public function run($config, $context)
    {

        // extract page from the file name
        $slug = basename($context->file, '.neon');

        // add a page template location
        if($slug === 'all'){
            $config['location'][] = [
                [
                    'param' => 'user_form',
                    'operator' => '==',
                    'value' => $slug,
                ]
            ];
        }else{
            $config['location'][] = [
                [
                    'param' => 'user_role',
                    'operator' => '==',
                    'value' => $slug,
                ]
            ];
        }

        $group = new Group($this->createGroupId($context, 'user-' . $slug), $config, new FieldFactory($this->defaults));
        $group->setup();

    }

}