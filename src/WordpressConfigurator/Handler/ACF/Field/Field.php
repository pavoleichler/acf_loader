<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

use WordpressConfigurator\Handlers\ACF\Item;

class Field extends Item implements IField
{

    protected $keyPrefix = 'field_';

    protected $groupId;
    protected $name;

    protected $defaults = [];

    public function __construct($config, $name = null, $groupId = null, $defaults = []) {

        // allow label shortcuts
        if (is_string($config)){
            $config = [
                'label' => $config
            ];
        }

        parent::__construct($config);

        $this->name = $name;
        $this->groupId = $groupId;
        $this->defaults = $defaults + $this->defaults;

    }

    protected function format()
    {

        if (isset($this->config['show_if'])){
            $showIf = $this->config['show_if'];
            if ($this->isAssociativeArray($showIf)){
                $showIf = [$showIf];
            }
            foreach($showIf as $condition){
                $this->addCondition($condition);
            }
        }

        return $this->config;

    }

    protected function defaults()
    {

        return $this->defaults + [
            'key' => $this->createUniqueKey($this->name . '|' . $this->groupId),
            'name' => $this->name,
            'label' => '',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
        ];

    }

    protected function addCondition($condition)
    {

        // init conditional logic
        if ( empty($this->config['conditional_logic']) or ! is_array($this->config['conditional_logic']) or ! is_array($this->config['conditional_logic'][0])) {
            $this->config['conditional_logic'] = [[]];
        }

        // translate the field name
        $field = strpos($condition['field'], '|') !== false ? $condition['field'] : $condition['field'] . '|' . $this->groupId;
        $condition['field'] = $this->createUniqueKey($field);

        // append to conditional logic
        $this->config['conditional_logic'][0][] = $condition;

    }

    protected function isAssociativeArray($array)
    {
        if (array() === $array) return false;
        return array_keys($array) !== range(0, count($array) - 1);
    }

    protected function createUniqueKey($id)
    {
        return $this->keyPrefix . md5($this->groupId ? $this->groupId . '/' . $id : $id);
    }

}