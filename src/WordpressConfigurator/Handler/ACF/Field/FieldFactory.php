<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

class FieldFactory
{

    protected $defaults;

    public function __construct($defaults = [])
    {

        $this->defaults = $defaults;

    }

    public function create($config, $name = null, $group = null)
    {

        // allow shortcut notation of text fields
        if (is_string($config)){
            return $this->createFromString($config, $name, $group);
        }else{
            return $this->createFromArray($config, $name, $group);
        }

    }

    protected function createFromString($shortcut, $name = null, $group = null)
    {

        // config
        $config = [
            'type' => 'text',
        ];

        // parse
        preg_match('/^(.*?)[\s]*(<(.+?)>)?[\s]*([+*]*)$/i', $shortcut, $matches);
        $config['label'] = $matches[1];
        $type = $matches[3];
        $modifiers = $matches[4];

        // modifiers
//        if (strpos($modifiers, '++') !== false){
//            $config['type'] = 'wysiwyg';
//        }elseif(strpos($modifiers, '+') !== false){
//            $config['type'] = 'textarea';
//        }
        if (strpos($modifiers, '*') !== false){
            $config['required'] = true;
        }

        // types
        if ($type){
            switch(strtolower($type)){
                case 'i':
                case 'img':
                    $config['type'] = 'image';
                    break;
                case 't':
                    $config['type'] = 'text';
                    break;
                case 'ta':
                    $config['type'] = 'textarea';
                    break;
                case 'w':
                    $config['type'] = 'wysiwyg';
                    break;
                default:
                    $config['type'] = $type;
                    break;
            }
        }

        // create from config
        return $this->createFromArray($config, $name, $group);

    }

    protected function createFromArray($config, $name = null, $group = null)
    {

        // switch by type
        switch ($config['type']){
            case 'true_false':
                return new TrueFalse($config, $name, $group, $this->getDefaultsByType('true_false'));
            case 'text':
                return new Text($config, $name, $group, $this->getDefaultsByType('text'));
            case 'textarea':
                return new Textarea($config, $name, $group, $this->getDefaultsByType('textarea'));
            case 'number':
                return new Number($config, $name, $group, $this->getDefaultsByType('number'));
            case 'tab':
                return new Tab($config, $name, $group, $this->getDefaultsByType('tab'));
            case 'file':
                return new File($config, $name, $group, $this->getDefaultsByType('file'));
            case 'image':
                return new Image($config, $name, $group, $this->getDefaultsByType('image'));
            case 'repeater':
                return new Repeater($config, $this, $name, $group, $this->getDefaultsByType('repeater'));
            case 'group':
                return new Group($config, $this, $name, $group, $this->getDefaultsByType('group'));
            case 'flexible_content':
                return new FlexibleContent($config, $this, $name, $group, $this->getDefaultsByType('flexible_content'));
            default:
                return new Field($config, $name, $group, !empty($config['type']) ? $this->getDefaultsByType($config['type']) : []);
        }

    }

    protected function getDefaultsByType($type)
    {

        return isset($this->defaults[$type]) ? $this->defaults[$type] : [];

    }

}