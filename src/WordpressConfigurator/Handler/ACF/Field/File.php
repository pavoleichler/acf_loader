<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

class File extends Field
{

    protected $defaults = [
        'type' => 'file',
        'return_format' => 'url',
        'library' => 'all',
        'min_size' => '',
        'max_size' => '',
        'mime_types' => '',
    ];

}