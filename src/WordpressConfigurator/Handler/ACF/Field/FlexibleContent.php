<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

class FlexibleContent extends Field
{

    protected $defaults = [
        'type' => 'flexible_content',
        'button_label' => 'Add new content',
        'min' => '',
        'max' => '',
        'layouts' => []
    ];

    /**
     * @var FieldFactory
     */
    protected $fieldFactory;

    public function __construct($config, FieldFactory $fieldFactory, $name = null, $groupId = null, $defaults = [])
    {
        parent::__construct($config, $name, $groupId, $defaults);

        $this->fieldFactory = $fieldFactory;

    }

    public function getSettings()
    {

        // get settings
        $settings = parent::getSettings();

        // overwrite subfields
        $settings['layouts'] = [];
        foreach ($this->config['layouts'] as $fieldId => $config){
            $field = new FlexibleContentLayout($config, $this->fieldFactory, $fieldId, $this->groupId. '/' . $this->name);
            $settings['layouts'][] = $field->getSettings();
        }

        return $settings;

    }

}