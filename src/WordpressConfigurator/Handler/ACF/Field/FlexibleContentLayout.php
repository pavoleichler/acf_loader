<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

class FlexibleContentLayout extends Field
{

    protected $defaults = [
        'display' => 'block',
        'sub_fields' => []
    ];

    /**
     * @var FieldFactory
     */
    protected $fieldFactory;

    public function __construct($config, FieldFactory $fieldFactory, $name = null, $groupId = null)
    {
        parent::__construct($config, $name, $groupId);

        $this->fieldFactory = $fieldFactory;
    }

    public function getSettings()
    {

        // get settings
        $settings = parent::getSettings();

        // overwrite subfields
        $settings['sub_fields'] = [];
        foreach ($this->config['sub_fields'] as $fieldId => $config){
            $field = $this->fieldFactory->create($config, $fieldId, $this->groupId . '/' . $this->name);
            $settings['sub_fields'][] = $field->getSettings();
        }

        return $settings;

    }

}