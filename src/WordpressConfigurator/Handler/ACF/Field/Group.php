<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

class Group extends Field
{

    protected $defaults = [
        'type' => 'group',
        'layout' => 'table',
        'sub_fields' => []
    ];

    /**
     * @var FieldFactory
     */
    protected $fieldFactory;

    public function __construct($config, FieldFactory $fieldFactory, $name = null, $groupId = null, $defaults = [])
    {
        parent::__construct($config, $name, $groupId, $defaults);

        $this->fieldFactory = $fieldFactory;

    }

    public function getSettings()
    {

        // get settings
        $settings = parent::getSettings();

        // overwrite subfields
        $settings['sub_fields'] = [];
        foreach ($this->config['sub_fields'] as $fieldId => $config){
            $field = $this->fieldFactory->create($config, $fieldId, $this->groupId . '/' . $this->name);
            $settings['sub_fields'][] = $field->getSettings();
        }

        return $settings;

    }

}