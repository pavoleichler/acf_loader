<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

interface IField
{

    /**
     * @return array
     */
    public function getSettings();

}