<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

class Image extends Field
{

    protected $defaults = [
        'type' => 'image',
        'return_format' => 'url',
        'preview_size' => 'full',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ];

}