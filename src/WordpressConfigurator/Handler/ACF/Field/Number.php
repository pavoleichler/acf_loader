<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

class Number extends Field
{

    protected $defaults = [
        'type' => 'number',
        'default_value' => '',
        'min' => '',
        'max' => '',
        'step' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
    ];

}