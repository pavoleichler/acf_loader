<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

class Tab extends Field
{

    protected $defaults = [
        'type' => 'tab',
        'placement' => 'top',
        'endpoint' => 0,
    ];

}