<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

class Text extends Field
{

    protected $defaults = [
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
    ];

}