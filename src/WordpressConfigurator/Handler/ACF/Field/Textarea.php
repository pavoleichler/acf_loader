<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

class Textarea extends Field
{

    protected $defaults = [
        'type' => 'textarea',
        'default_value' => '',
        'placeholder' => '',
        'maxlength' => '',
        'rows' => 4,
        'new_lines' => '',
    ];

}