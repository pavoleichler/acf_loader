<?php

namespace WordpressConfigurator\Handlers\ACF\Field;

class TrueFalse extends Field
{

    protected $defaults = [
        'type' => 'true_false',
        'message' => '',
        'default_value' => 0,
        'ui' => 0,
        'ui_on_text' => '',
        'ui_off_text' => '',
    ];

}