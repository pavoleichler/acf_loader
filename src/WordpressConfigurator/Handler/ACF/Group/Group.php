<?php

namespace WordpressConfigurator\Handlers\ACF\Group;

use WordpressConfigurator\Handlers\ACF\Field\FieldFactory;
use WordpressConfigurator\Handlers\ACF\Item;

class Group extends Item
{

    /**
     * @var FieldFactory
     */
    protected $fieldFactory;

    protected $id;
    protected $keyPrefix = 'group_';

    public function __construct($id, $config, FieldFactory $fieldFactory) {
        parent::__construct($config);

        $this->id = $id;
        $this->fieldFactory = $fieldFactory;

    }

    protected function defaults()
    {

        return [
            'key' => $this->createUniqueKey($this->id),
            'title' => null,
            'fields' => [],
            'location' => [],
            'menu_order' => 0,
            'position' => 'acf_after_title',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ];

    }

    public function setup()
    {

        if (function_exists('acf_add_local_field_group')){
            acf_add_local_field_group($this->getSettings());
        }

    }

    public function dump()
    {

        $settings = $this->getSettings();

        // fields array
        $fields = [];
        foreach($settings['fields'] as $key => $values){
            $fields[$values['name']] = "{$values['label']} ({$values['type']})";
        }

        // dump
        dump([
            'title' => $settings['title'],
            'fields' => $fields,
            'location' => $settings['location']
        ]);

    }

    public function getSettings()
    {

        // get settings
        $settings = parent::getSettings();

        // overwrite fields
        $settings['fields'] = [];
        $this->config['fields'] = $this->config['fields'] ?? [];
        foreach ($this->config['fields'] as $fieldId => $config){
            $field = $this->fieldFactory->create($config, $fieldId, $this->id);
            $settings['fields'][] = $field->getSettings();
        }

        return $settings;

    }

}