<?php

namespace WordpressConfigurator\Handlers\ACF;

class Item
{

    protected $config;

    protected $keyPrefix = '';

    public function __construct($config) {

        $this->config = $config;

    }

    protected function defaults()
    {
        return [];
    }

    protected function format()
    {

        return $this->config;

    }

    protected function createUniqueKey($id)
    {

        return $this->keyPrefix . md5($id);

    }

    public function getSettings()
    {

        return $this->format() + $this->defaults();

    }

}