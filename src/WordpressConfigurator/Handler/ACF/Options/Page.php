<?php

namespace ACFLoader\Configurator\Options;

class Page
{

    protected $postId;
    protected $slug;
    protected $title;
    protected $icon;

    public function __construct($slug, $postId = null, $title = null, $icon = null)
    {

        $this->slug = $slug;
        $this->postId = $postId ?: $slug;
        $this->title = $title ?: $this->createTitleFromSlug($slug);
        $this->icon = $icon ?: 'dashicons-admin-generic';

    }

    protected function createTitleFromSlug($slug)
    {

        $words = explode('-', $slug);
        array_walk($words, function(&$word){
            $word = ucfirst($word);
        });

        return implode(' ', $words);

    }

    public function setup()
    {

        add_action( 'after_setup_theme', [$this, 'register']);

    }

    public function register()
    {

        // create a new options page if it does not exist yet
        if (function_exists('acf_add_options_page') and !acf_get_options_page($this->slug)){

            $wpmlLang = apply_filters( 'wpml_current_language', null);

            acf_add_options_page(array(
                'page_title' => $this->title,
                'menu_title' => $this->title,
                'menu_slug'  => $this->slug,
                'capability' => 'edit_posts',
                'autoload'   => true,
                'post_id'    => $this->postId . ($wpmlLang ? '_' . $wpmlLang : ''),
                'redirect'   => false,
                'icon_url'    => $this->icon,
            ));

        }

    }

}