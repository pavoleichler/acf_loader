<?php

namespace WordpressConfigurator\Handlers;

interface IHandler
{

    /**
     * @param $config
     *
     * @return array
     */
    public function run($config, $context);

}