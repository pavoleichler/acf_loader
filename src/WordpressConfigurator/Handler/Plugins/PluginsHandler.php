<?php

namespace WordpressConfigurator\Handlers\Plugins;

use WordpressConfigurator\Handlers\IHandler;

class PluginsHandler implements IHandler
{

    protected $config;
    protected $context;

    public function run($config, $context)
    {

        $this->config = $this->parseConfig($config);
        $this->context = $context;

        add_action('tgmpa_register', [$this, 'register']);

    }

    protected function replaceVariables($value)
    {

        return preg_replace_callback('/%themeDir%/', function(){ return get_template_directory(); }, $value);

    }

    protected function parseConfig($config)
    {

        // replace variables
        array_walk_recursive($config, function(&$value){
            $value = $this->replaceVariables($value);
        });

        // reformat plugins
        $plugins = [];
        foreach ($config['plugins'] as $slug => $plugin){
            $plugin['slug'] = $slug;
            $plugins[] = $plugin;
        }
        $config['plugins'] = $plugins;

        return $config;

    }

    public function register()
    {

        // default settings
        $settings = [
            'id'           => $this->context->theme,   // Unique ID for hashing notices for multiple instances of TGMPA.
            'has_notices'  => true,                    // Show admin notices or not.
            'dismissable'  => false,                   // If false, a user cannot dismiss the nag message.
            'is_automatic' => true,                    // Automatically activate plugins after installation or not.
        ];

        // register
        tgmpa($this->config['plugins'], $settings);

    }

}