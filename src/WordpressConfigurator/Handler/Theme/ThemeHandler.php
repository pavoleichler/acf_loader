<?php

namespace WordpressConfigurator\Handlers\Theme;

use WordpressConfigurator\Handlers\IHandler;

class ThemeHandler implements IHandler
{

    const CRON_SYSTEM = 'system';

    protected $config;
    protected $context;

    public function run($config, $context)
    {

        $this->config = $this->parseConfig($config);
        $this->context = $context;

        $this->createConstants();

        add_action('after_setup_theme', [$this, 'register']);

    }

    protected function replaceVariables(&$value)
    {

        // none

    }

    protected function expandImageShortcode($image)
    {
        $valid = preg_match('/([\d]+)x([\d]+)(c)?((?<=c)[xrl](?=[xtb]))?((?<=[xrl])[xtb])?/i', $image, $matches);

        if(!$valid ){
            throw new \Exception('Invalid image shortcode');
        }

        if(!empty($matches[4])){
            switch ($matches[4]) {
                case 'x':
                    $x_crop_position = 'center';
                    break;
                case 'r':
                    $x_crop_position = 'right';
                    break;
                case 'l':
                    $x_crop_position = 'left';
                    break;
            }
            switch ($matches[5]) {
                case 'x':
                    $y_crop_position = 'center';
                    break;
                case 't':
                    $y_crop_position = 'top';
                    break;
                case 'b':
                    $y_crop_position = 'bottom';
                    break;
            }
            return [
                'width' => $matches[1],
                'height' => $matches[2],
                'crop' => array($x_crop_position, $y_crop_position),
            ];
        }else{
            return [
                'width' => $matches[1],
                'height' => $matches[2],
                'crop' => !empty($matches[3]),
            ];
        }
    }

    protected function parseConfig($config)
    {

        // replace variables
        array_walk_recursive($config, function(&$value){
            $this->replaceVariables($value);
        });

        // reformat images
        $config['images'] = $config['images'] ?: [];
        foreach ($config['images'] as &$image){
            if (is_string($image)){
                $image = $this->expandImageShortcode($image);
            }
        }

        return $config;

    }

    protected function createConstants(){

        if (empty($this->config['constants'])) {
            return;
        }

        foreach($this->config['constants'] as $name => $value){
            define($name, $value);
        }

    }

    public function register()
    {

        /*
        * Make theme available for translation.
        * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
        * If you're building a theme based on Twenty Seventeen, use a find and replace
        * to change 'twentyseventeen' to the name of your theme in all the template files.
        */
        load_theme_textdomain($this->config['id']);

        /*
        * Let WordPress manage the document title.
        * By adding theme support, we declare that this theme does not use a
        * hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
        if (!empty($this->config['title-tag'])){
            add_theme_support('title-tag');
        }

        /*
        * Enable support for Post Thumbnails on posts and pages.
        *
        * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
        */
        add_theme_support('post-formats', !empty($this->config['post-formats']) ? $this->config['post-formats'] : []);
        add_theme_support('post-thumbnails');
        foreach($this->config['images'] as $id => $image){
            add_image_size($this->config['id'] . '-' . $id, $image['width'], $image['height'], $image['crop']);
            add_filter( 'image_size_names_choose', function ( $sizes ) use ($id) {
                return array_merge( $sizes, array(
                    $this->config['id'] . '-' . $id => ucwords(implode(' ', explode('-', $this->config['id']))) . ' ' . ucwords(implode(' ', explode('-', $id)))
                ));
            });
        }

        // media upload criteria
        add_filter( 'wp_handle_upload_prefilter', [$this, 'requireMediaSize']);

        // register menus
        add_theme_support('menus');
        if ($this->config['menus']){
            foreach ($this->config['menus'] as $id => $title){
                register_nav_menus( array(
                    $id => __($title, $this->config['id'])
                ) );
            }
        }

        // register user roles
        if ($this->config['roles']){
            foreach ($this->config['roles'] as $id => $role) {
                add_role($id, $role['name'], get_role($role['capabilities'])->capabilities);
            }
        }

        // guttenberg
        add_filter('use_block_editor_for_post_type', function ($currentStatus, $postType) {

            // not provided
            if (!isset($this->config['guttenberg'])){
                return $currentStatus;
            }

            // provided for this post type
            if(isset($this->config['guttenberg']['post-types']) and
               isset($this->config['guttenberg']['post-types'][$postType])){
                return (bool) $this->config['guttenberg']['post-types'][$postType];
            }

            // use default value
            return isset($this->config['guttenberg']['default']) ? (bool) $this->config['guttenberg']['default'] : $currentStatus;

        }, 10, 2);

        // wysiwyg toolbars
        add_filter( 'acf/fields/wysiwyg/toolbars', function($toolbars){

            if (isset($this->config['wysiwyg']) and isset($this->config['wysiwyg']['toolbars'])){
                $toolbars += $this->config['wysiwyg']['toolbars'];
            }

            return $toolbars;
        });

        // other
        add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));

        // cron settings
        if ($this->config['cron'] === self::CRON_SYSTEM){
            $this->useSystemCron();
        }

    }

    protected function useSystemCron()
    {

        // disable default WP cron
        define('DISABLE_WP_CRON', true);

        // disable image thumbnail generation through direct upload
        // allow CLI media regeneration only
        add_filter('intermediate_image_sizes_advanced', function($sizes){
            if (php_sapi_name() !== 'cli'){
                return [];
            }
            return $sizes;
        });

    }

    public function requireMediaSize($file)
    {

        // load image dimensions
        $dimensions = getimagesize($file['tmp_name']);

        // load settings
        $maxWidth = $this->config['media']['images']['max-width'] ?? null;
        $maxHeight = $this->config['media']['images']['max-height'] ?? null;

        // require
        if ($dimensions and
            (($maxWidth and $dimensions[0] > $maxWidth) or
             ($maxHeight and $dimensions[1] > $maxHeight))){
            $file['error'] = "Image dimensions must not exceed {$maxWidth}x{$maxHeight} px.";
        }

        return $file;

    }

}