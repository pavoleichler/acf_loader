<?php

namespace WordpressConfigurator;

use Nette\Utils\Finder;
use WordpressConfigurator\Handlers\IHandler;

class WordpressConfigurator
{

    /**
     * @var IFileParser
     */
    protected $parser;

    /**
     * @var arraz
     */
    protected $context;

    public function __construct($theme, IFileParser $parser = null) {

        $this->parser = $parser ?: new FileParser\Neon();
        $this->context = (object) [
            'theme' => $theme,
        ];

    }

    public function addFile($file, $handler)
    {

        // prepare context
        $context = (object) (['file' => $file] + (array) $this->context);

        if ($handler instanceof IHandler){
            return $handler->run($this->parser->parse($file), $context);
        }elseif (is_callable($handler)){
            return $handler($this->parser->parse($file), $context);
        }else{
            throw new \Exception('Invalid handler');
        }

    }

    public function addFolder($folder, $handler, $recursive = false)
    {

        // ignore missing folders
        if (!file_exists($folder)){
            return;
        }

        if ($recursive){
            $finder = Finder::findFiles('*.neon')->from($folder);
        }else{
            $finder = Finder::findFiles('*.neon')->in($folder);
        }

        // loop through files
        foreach ($finder as $path => $file) {
            // parse using the folder defined parser
            $this->addFile($path, $handler);
        }

    }

    /**
     * @return FileParser\Neon|IFileParser|null
     */
    public function getParser()
    {
        return $this->parser;
    }

}