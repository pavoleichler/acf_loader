<?php

if (!class_exists('WordpressConfigurator\WordpressConfigurator')) {

    require __DIR__ . '/WordpressConfigurator/WordpressConfigurator.php';
    require __DIR__ . '/WordpressConfigurator/FileParser/IFileParser.php';
    require __DIR__ . '/WordpressConfigurator/FileParser/Neon.php';
    require __DIR__ . '/WordpressConfigurator/Handler/IHandler.php';
    require __DIR__ . '/WordpressConfigurator/Handler/Plugins/PluginsHandler.php';
    require __DIR__ . '/WordpressConfigurator/Handler/Theme/ThemeHandler.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Item.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/ACFHandler.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/ACFOptionsHandler.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/ACFPageHandler.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/ACFCustomPostTypeHandler.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/ACFPostTypeHandler.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/ACFBlockTypeHandler.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/ACFTaxonomyHandler.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/ACFAttachmentHandler.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Group/Group.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/IField.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/Field.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/Text.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/Textarea.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/TrueFalse.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/Number.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/FieldFactory.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/Tab.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/File.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/Image.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/Repeater.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/FlexibleContent.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Field/FlexibleContentLayout.php';
    require __DIR__ . '/WordpressConfigurator/Handler/ACF/Options/Page.php';

}